# COVID - Summary
Personal notes by Jan Miranda

**Disclaimer:** I am not a medical professional. Information contained within is for my personal interest in understanding COVID-19.

# Table of Contents
[TOC]


---

# COVID-19 aka SARS-CoV-2
SARS-CoV-2 (Severe acute respiratory syndrome coronavirus 2)

![a747324e5b8b81b03a84535594417e5f.png](images/941993fbf30045ebb52c3edc4b5a359e.png)
- got it's name because of the projections that make it look like the Sun (corona)
- Sprotein - on the end of the projections - bind to the ACE2 receptors on the cells in the lung (Type 2 pneumocytes) and GI tract
- Sprotein allows messenger RNA from the virus to enter the cells

# Difference between COVID-19 and influenza
- unlike seasonal influenza, COVID-19 is more deadly, is much more transmissible, has a higher R0, and effects more organs in ways we don't fully understand yet. 
- The key to what makes COVID-19 so dangerous is how it attacks human tissues that express ACE2 (Angiotensin-converting enzyme 2).
- Some tissues that express ACE2 include the lungs, heart, kidneys, intestines, brain, and testicles.

## Similarities:
- COVID-19 and influenza viruses have a similar disease presentation. That is, they both cause respiratory disease, which presents as a wide range of illness from asymptomatic or mild through to severe disease and death.
- both viruses are transmitted by contact, droplets and fomites (objects - clothes, utensils, furnature, etc).

## Differences:
- **speed of transmission**
	- Influenza has a shorter median incubation period and a shorter serial interval (the time between successive cases) than COVID-19 virus. This means that influenza can spread faster than COVID-19
	- **COVID-19 - serial median interval 5-6 days (but up to 14 days)**
	- **influenza - serial interval is 3 days.**
- **reproductive number** (R0) – the number of secondary infections generated from one infected individual – is understood to be between 2 and 2.5 for COVID-19 virus, higher than for influenza. However, estimates for both COVID-19 and influenza viruses are very context and time-specific, making direct comparisons more difficult. 
- **morbidity** 
	- COVID-19 - severe and critical infection are higher than influenza infection
		- 80% of infections are mild or asymptomatic, 
		- 15% are severe infection, requiring oxygen
		- 5% are critical infections, requiring ventilation.
	- **COVID-19 - crude mortality ratio** (the number of reported deaths divided by the reported cases) is between 3-4%
	- influenze - mortality ratio below 0.1%
- **Population at risk**
	- **influenza** - children, pregnant women, elderly, those with underlying chronic medical conditions and those who are immunosuppressed
	- **COVID-19** - older age and underlying conditions increase the risk for severe infection (such as obesity, diabetes, immunosuppressed individuals)

# How to protect yourself:
- Suppliments:
	- **Vitamins**: C, D, Zinc, Thiamine
- avoid sugar and simple carbohydrates
- sufficient sleep
- exercise
- low BMI

# Organs COVID-19 attacks
### ACE2 (Angiotensin-converting enzyme 2)
- ACE2 the key host cellular receptor of SARS-CoV-2, has been identified in multiple organs
- The mechanism for SARS-CoV-2 infection is the requisite binding of the virus to the membrane-bound form of angiotensin-converting enzyme 2 (ACE2) and internalization of the complex by the host cell.
Tissues that express ACE2 include lungs, heart, kidneys, intestines, brain, and testicles.

### Evidence for Gastrointestinal Infection of SARS-CoV-2
- The viral infection causes a series of respiratory illnesses, including severe respiratory syndrome, indicating that the virus most likely infects respiratory epithelial cells and spreads mainly via respiratory tract from human to human. However, viral target cells and organs have not been fully determined, impeding our understanding of the pathogenesis of the viral infection and viral transmission routes.
- It has been proven that SARS-CoV-2 uses angiotensin-converting enzyme (ACE) 2 as a viral receptor for entry process. ACE2 messenger RNA is highly expressed and stabilized by B0AT1 in gastrointestinal system

## Infection of the Heart
- The ACE2 expression in human heart indicates new potential mechanism of heart injury among patients infected with SARS-CoV-2
- Cardiac injury is a prevalent complication of severe patients, exacerbating the disease severity in coronavirus disease 2019 (COVID-19) patients. Angiotensin-converting enzyme 2 (ACE2), the key host cellular receptor of SARS-CoV-2, has been identified in multiple organs

### COVID-19, ACE2, and the **cardiovascular** consequences.
- The novel SARS coronavirus SARS-CoV-2 pandemic may be particularly deleterious to patients with underlying cardiovascular disease (CVD).
- The mechanism for SARS-CoV-2 infection is the requisite binding of the virus to the membrane-bound form of angiotensin-converting enzyme 2 (ACE2) and internalization of the complex by the host cell.
- Tissues that express ACE2 include lungs, heart, kidneys, intestines, brain, and testicles.

![7559bc4900da1302648e807e96f8f4d9.png](images/fa15c932ae114ee8a0dbda22b6ed8a69.png)
Tissue distribution of ACE2 receptors in humans. Viremia (A) disseminates the COVID-19 virus throughout the body via the bloodstream (B). Neurotropism may occur via circulation and/or an upper nasal trancribrial route that enables the COVID-19 to reach the brain © and bind and engage with the ACE2 receptors (D, blue). COVID-19 docks on the ACE2 via spike protein (D, golden spikes). Shown are lungs, heart, kidneys, intestines, brain, and testicles that are well-known to express ACE2 receptors and are possible targets of COVID-19.

## Blood clots & Strokes
Thrombosis (blood clots), strokes, and myocardial infarctions are mysterious complications for some patients with COVID-19.  Dr. Seheult discusses a recent case study involving a 72-year-old with elevated d-dimer and plasma Von Willebrand factor, and goes on to illustrate a hypothesis for how downregulation of ACE-2 may result in oxidative stress. This process may put patients with underlying elevated levels of oxidative stress (cardiovascular diseases, diabetes, obesity, etc.) at the greatest risk for severe COVID-19 infection.

## Evidence of the COVID-19 Virus Targeting the Brain and the CNS
- https://www.ncbi.nlm.nih.gov/pmc/articles/PMC7094171/#ref3
- The brain has been reported to express ACE2 receptors that have been detected over **glial cells** and **neurons**, which makes them a potential target of COVID-19
- Previous studies have shown the ability of SARS-CoV to cause neuronal death in mice by invading the brain via the nose close to the olfactory epithelium.
![86f7803a06bc8b40daf9a2c6e4c633c1.png](images/2fd7cbc2e5334b8f93c8083d7ef5a1af.png)

# Notes

## Epidemiological

It is believed to have zoonotic origins and has close genetic similarity to bat coronaviruses, suggesting it emerged from a bat-borne virus.
 
Epidemiological studies estimate each infection results in 1.4 to 3.9 new ones when no members of the community are immune and no preventive measures taken. The virus is primarily spread between people through close contact and via respiratory droplets produced from coughs or sneezes. It mainly enters human cells by binding to the receptor angiotensin converting enzyme 2 (ACE2).

---

## Structural biology
Each SARS-CoV-2 virion is approximately 50–200 nanometres in diameter. Like other coronaviruses, SARS-CoV-2 has four structural proteins, known as the S (spike), E (envelope), M (membrane), and N (nucleocapsid) proteins; the N protein holds the RNA genome, and the S, E, and M proteins together create the viral envelope.

![4a7405d5ae476aa22e883165fbf1cd95.png](images/d7876c35346547db9d551d7aa2dfadb5.png)

---

## How COVID-19 attacks blood vessels
- endothelial cells have on their surface ACE2 receptors
- this causes inflamation for the endothelial layer
- under the endothelial cells is vWF (von Willebrand's factor)
- vWF is released when the endothelial cells are damaged
- **Factor VIII** (F8) - is in the lumen (in the blood vessel) - see clotting cascade below
- vWF acts on (F8) which converts (F10) to (F10a) - see clotting cascade below

![89aa4db875c559a34cd40ec118916940.png](images/601e00028c49425983e6cd4eb3a589d1.png)


### Clotting Cascade

![4915643bc4be147a9b2cf16240257488.png](images/1b288312fd6a41c2bf37584ecfa9c482.png)

